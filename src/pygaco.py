from window import  PyGacoWindow
from config import AppConfig



if __name__ == '__main__':
    config = AppConfig.load()
    window = PyGacoWindow(config)
    window.setup()
    window.run()

