import math


class Vector2D:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    @property
    def magnitude(self) -> float:
        return math.sqrt(self.x**2 + self.y**2)

    @property
    def angle(self) -> float:
        return math.atan(self.y / self.x)

    @property
    def angle_degree(self) -> float:
        return self.angle * 180 / math.pi

    def __len__(self) -> float:
        return self.magnitude

    def __mul__(self, x: float) -> 'Vector2D':
        return Vector2D(self.x * x, self.y * x)

    def __imul__(self, x: float) -> 'Vector2D':
        self.x *= x
        self.y *= x
        return self

    def __truediv__(self, x: float) -> 'Vector2D':
        return Vector2D(self.x / x, self.y / x)

    def __itruediv__(self, x: float) -> 'Vector2D':
        self.x /= x
        self.y /= x
        return self

    def normalized(self):
        return self.__class__(self.x / self.magnitude, self.y / self.magnitude)

    @classmethod
    def from_points(cls, a: tuple[float, float], b: tuple[float, float]):
        return cls(b[0] - a[0], b[1] - a[1])

    @classmethod
    def from_angle(cls, angle: float):
        """Create an unit vector from an angle (in radians).
        """
        x = math.cos(angle)
        y = math.sin(angle)
        return cls(x, y)

