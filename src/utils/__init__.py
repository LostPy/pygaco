from itertools import chain
from .vector import Vector2D


def iter_has_next(iterable) -> tuple:
    try:
        first = next(iterable)
        res = True
    except StopIteration:
        res = False
    return res, chain([first], iterable)

