import math
import random
from arcade import Window, Scene
import arcade

from config import AppConfig
from sprites import Colony, Ant, RedPheromone, BluePheromone, Pheromone, Food


class PyGacoWindow(Window):
    """Main window for the simulation."""

    def __init__(self, config: AppConfig):
        super().__init__(
            config.window['width'],
            config.window['height'],
            fullscreen=config.window['fullscreen'],
            update_rate=config.simulation['update_rate'],
            title=config.window['title']
        )
        self.app_config = config
        arcade.set_background_color(arcade.color.JET)

        Food.RADIUS = 10
        Ant.SPEED = config.simulation['ant_velocity'] * config.simulation['update_rate']
        self.map_path = config.simulation['map_path']
        self.map_layer_walls = config.simulation['map_layer_walls']

        self.scene = None
        self.map = None

        self.text_timer = None
        self.text_food_collected = None
        self.text_pause = None
        self.total_time = 0.
        self.last_pheromone_update = 0.
        self.mouse_food = None
        self.start = False
        self.pause = True

    def setup_colony(self, x, y):
        self.colony = Colony()
        self.colony.center_x = x
        self.colony.center_y = y
        self.scene.add_sprite("Colony", self.colony)

    def setup_source_food(self, x, y):
        radius = random.randint(15, 35)
        n = random.randint(2 * radius, 3 * radius)
        Food.create_source(self.scene['Foods'], x, y, n, radius)

    def setup_ants(self, n: int = 10):
        for i in range(n):
            self.scene.add_sprite("Ants", Ant(self.colony))

    def setup_text(self):
        self.text_timer = arcade.Text(
            "00:00",
            self.width - 80,
            self.height - 20,
            arcade.color.PLATINUM,
            bold=True,
        )
        self.text_food_collected = arcade.Text(
            "Food Collected: 0",
            30,
            self.height - 20,
            arcade.color.PLATINUM,
            13,
        )
        self.text_pause = arcade.Text(
            "Left Click to add a colony then left click to add food sources."
            " Escape to reset the simulation. Space to pause/resume.",
            30, 30,
            arcade.color.PLATINUM,
            14,
        )

    def setup_map(self):
        layer_options = {
            self.map_layer_walls: {
                'use_spatial_hash': True
            },
        }
        self.map = arcade.load_tilemap(
            self.map_path,
            self.app_config.simulation['map_tile_scaling'],
            layer_options
        )

    def reset_attributes(self):
        self.colony = None
        self.total_time = 0.
        self.last_pheromone_update = 0.
        self.pause = True
        self.start = False
        self.mouse_food = None

    def setup(self):
        """Setup the simulation."""

        self.reset_attributes()

        if self.map_path:
            self.setup_map()

        # setup scene
        if self.map is None:
            self.scene = Scene()
            self.scene.add_sprite_list(self.map_layer_walls, use_spatial_hash=True)
        else:
            self.scene = Scene.from_tilemap(self.map)
        self.scene.add_sprite_list("Colony", use_spatial_hash=True)
        self.scene.add_sprite_list("Pheromones")
        self.scene.add_sprite_list("BluePheromones")
        self.scene.add_sprite_list("RedPheromones")
        self.scene.add_sprite_list("Foods")
        self.scene['Pheromones'].capacity = 1000
        self.scene.add_sprite_list("Ants")

        self.setup_text()

    def update_ants(self, delta_time: float):
        redirection_angle = 2 * math.pi * random.random()
        change_y = abs(math.sin(redirection_angle) * Ant.SPEED)
        change_x = abs(math.cos(redirection_angle) * Ant.SPEED)

        for ant in self.scene['Ants']:
            ant.update(filter(ant.entity_in_circle_view, self.scene['Foods']),
                       filter(ant.entity_in_circle_view, self.scene['BluePheromones']),
                       filter(ant.entity_in_circle_view, self.scene['RedPheromones']))

            if arcade.check_for_collision(ant, ant.colony):
                if ant.food:
                    self.colony.food_collected += 1
                    ant.food = None
                    ant.internal_clock = 0.
                    ant.inverse_move_direction()
                continue

            elif self.map is None:
                if ant.top >= self.height:
                    ant.change_y = -change_y

                elif ant.bottom <= 0:
                    ant.change_y = change_y

                if ant.left <= 0:
                    ant.change_x = change_x

                elif ant.right >= self.width:
                    ant.change_x = -change_x
                # coeff to normalize the speed
                fix_speed = Ant.SPEED / math.sqrt(ant.change_x**2 + ant.change_y**2)
                ant.change_x = fix_speed * ant.change_x
                ant.change_y = fix_speed * ant.change_y

            else:
                wall_collisions = arcade.check_for_collision_with_list(
                    ant,
                    self.scene[self.map_layer_walls]
                )
                if len(wall_collisions) > 0:
                    ant.inverse_move_direction()

            ant.on_update(delta_time)

            if self.total_time == self.last_pheromone_update:
                ant.make_pheromone(self.scene)
    
    def update_texts(self):
        self.text_timer.text = f"{self.total_time // 60:02.0f}:{self.total_time % 60:02.0f}"
        self.text_food_collected.text = f"Food collected: {self.colony.food_collected:0d}"

    def update_pheromones(self):
        for pheromone in self.scene['Pheromones']:
            pheromone.intensity -= 1
            if pheromone.intensity <= 0:
                pheromone.kill()

    def on_update(self, delta_time: float):
        """Update simulation."""
        if not self.pause:
            self.total_time += delta_time
            self.scene.update(["Colony", "Foods", "Pheromones"])

            if self.total_time - self.last_pheromone_update > 0.4 * Ant.SPEED:
                self.update_pheromones()
                self.last_pheromone_update = self.total_time

            self.update_ants(delta_time)
            self.update_texts()

    def on_draw(self):
        """Render simulation."""
        self.clear()
        if self.map:
            for layer in self.map.sprite_lists.values():
                layer.draw()
        self.scene.draw(["Colony", "Pheromones", "Foods", "Ants"])
        self.text_timer.draw()
        self.text_food_collected.draw()
        if self.pause:
            self.text_pause.draw()

    def on_mouse_press(self, x, y, button, modifier):
        if button == arcade.MOUSE_BUTTON_LEFT:
            if self.pause and self.colony:
                self.setup_source_food(x, y)
            elif self.pause:
                self.setup_colony(x, y)
            else:
                self.scene.add_sprite('Foods', Food(x, y, radius=1))

        elif not self.pause and button == arcade.MOUSE_BUTTON_RIGHT:
            self.mouse_food = Food(x, y, radius=1)
            self.scene.add_sprite("Foods", self.mouse_food)

    def on_mouse_release(self, x, y, button, modifier):
        if not self.pause and self.mouse_food and button == arcade.MOUSE_BUTTON_RIGHT:
            self.mouse_food.kill()
            self.mouse_food = None

    def on_mouse_motion(self, x, y, dx, dy):
        if self.mouse_food is not None:
            self.mouse_food.center_x = x
            self.mouse_food.center_y = y

    def on_key_press(self, key, modifier):
        match key:
            case arcade.key.ESCAPE:
                self.setup()
                return
            case arcade.key.SPACE:
                if not self.start and self.pause:
                    self.start = True
                    self.setup_ants(self.app_config.simulation['ant_count'])
                self.pause = not self.pause

