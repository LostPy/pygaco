import random
import math
from arcade import Sprite
from utils import Vector2D, iter_has_next
from . import Entity, Colony, Pheromone, BluePheromone, RedPheromone, Food


class Ant(Entity):

    SPEED = 2 
    ANGLE_VIEW = 220  # angle of vision in degree
    DISTANCE_VIEW = 300  # Distance of the vision in pixels
    DISTANCE_GET_FOOD = 4

    def __init__(self, colony: Colony, scale: float = 0.04):
        super().__init__("sprites", "ant", scale=scale)
        self.colony = colony
        self.food: Food = None
        self.internal_clock: float = 0.
        self.center_x = self.colony.center_x + random.randint(-10, 10)
        self.center_y = self.colony.center_y + random.randint(-10, 10)
        init_direction = 2 * math.pi * random.random()
        self.change_x = self.SPEED * math.cos(init_direction)
        self.change_y = self.SPEED * math.sin(init_direction)
        self.target: Sprite = None

    def move_with_vector(self, vector: Vector2D):
        """Move the ant in the direction of the vector with the speed of ant.
        """
        vector = vector.normalized() * self.SPEED
        self.change_x = vector.x
        self.change_y = vector.y

    def inverse_move_direction(self):
        self.change_x = -self.change_x
        self.change_y = -self.change_y
        self.angle = (self.angle + 180) % 360

    def search_pheromone(self, pheromones: list) -> bool:
        pheromones_visible = self.entities_in_view(pheromones, minimal_distance=20)
        try:
            # set pheromone as target if not error
            pheromone = self.pheromone_hightest_intensity(pheromones_visible)
            self.move_with_vector(
                Vector2D.from_points(
                    (self.center_x, self.center_y),
                    (pheromone.center_x, pheromone.center_y),
            ))
        except ValueError:
            # if none is visible, move randomly
            if random.uniform(0, 1) > 0.97:
                angle_direction = 2 * math.pi * random.random()
                self.change_x = self.SPEED * math.cos(angle_direction)
                self.change_y = self.SPEED * math.sin(angle_direction)
            return False
        return True

    def search_food(self, foods: list) -> bool:
        foods_visible = self.entities_in_view(foods, minimal_distance=self.DISTANCE_GET_FOOD)
        self.target = self.entity_nearest(foods_visible)
        return True  # if not found raised a ValueError

    def search_colony(self) -> bool:
        vector = Vector2D.from_points(
            (self.center_x, self.center_y),
            (self.colony.center_x, self.colony.center_y),
        )
        if vector.magnitude <= 50 + self.colony.width / 2:
            self.move_with_vector(vector)
            return True
        return False

    def update(self, foods, blue_pheromones, red_pheromones):
        if self.target is not None:
            if not self.target.on_map:
                # If the food was taken by an other ant
                self.target = None
                return self.update(foods, blue_pheromones, red_pheromones)

            vector = Vector2D.from_points(
                (self.center_x, self.center_y),
                (self.target.center_x, self.target.center_y)
            )
            self.move_with_vector(vector)

            target_is_visible = self.entity_in_view(self.target, self.DISTANCE_GET_FOOD)
            if not target_is_visible and vector.magnitude <= self.DISTANCE_GET_FOOD:
                self.food = self.target
                self.inverse_move_direction()
                self.target.kill()
                self.target = None
                self.internal_clock = 0.

        else:
            if self.food is not None:
                if not self.search_colony():
                    # search blue pheromones
                    self.search_pheromone(blue_pheromones)

            else:
                # seach food
                try:
                    self.search_food(foods)
                except ValueError:
                    # search red pheromone
                    self.search_pheromone(red_pheromones)

        vector_speed = Vector2D(self.change_x, self.change_y)
        self.angle = (vector_speed.angle_degree + 180) % 360
        super().update()

    def on_update(self, delta_time: float):
        self.internal_clock += delta_time
        super().on_update(delta_time)

    def entity_in_view(self, entity: Sprite, minimal_distance: int = 10) -> bool:
        # pos of sensor (or eyes) of the ant) -> front of ant
        angle = self.angle * math.pi / 180
        sensor_pos = (
            self.center_x + math.cos(angle) * self.width,
            self.center_y + math.sin(angle) * self.height,
        )
        vector = Vector2D.from_points(
            sensor_pos,
            (entity.center_x, entity.center_y)
        )
        if minimal_distance <= vector.magnitude <= self.DISTANCE_VIEW:
            # The min boundary is to "remove" entities reached (and doesn't block the ant)
            boundary_angles = (
                (self.angle - self.ANGLE_VIEW / 2) % 360,
                (self.angle + self.ANGLE_VIEW / 2) % 360,
            )
            if boundary_angles[0] <= vector.angle_degree <= boundary_angles[1]:
                return True
        return False

    def entities_in_view(self, entities: list[Sprite], minimal_distance: int = 10) -> 'iterable[Sprite]':
        return filter(
            lambda entity: self.entity_in_view(entity, minimal_distance),
            entities
        )

    def get_distance_with_entity(self, entity: Sprite) -> float:
        return Vector2D.from_points(
            (self.center_x, self.center_y),
            (entity.center_x, entity.center_y),
        ).magnitude

    def entity_nearest(self, entities: list[Sprite]) -> Sprite:
        return min(entities, key=self.get_distance_with_entity)

    def entity_in_circle_view(self, entity: Sprite) -> bool:
        vector = Vector2D.from_points(
            (self.center_x, self.center_y),
            (entity.center_x, entity.center_y),
        )
        return vector.magnitude <= self.DISTANCE_VIEW

    def pheromone_hightest_intensity(self, pheromones: list[Pheromone]) -> Pheromone:
        return max(pheromones, key=lambda x: x.intensity)

    def make_pheromone(self, scene) -> Pheromone:
        if self.food:
            pheromone = RedPheromone.from_ant(self)
            scene.add_sprite("RedPheromones", pheromone)
        else:
            pheromone = BluePheromone.from_ant(self)
            scene.add_sprite("BluePheromones", pheromone)
        scene.add_sprite("Pheromones", pheromone)

