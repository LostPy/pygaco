from arcade import Sprite, load_texture


class Entity(Sprite):

    BASE_PATH = "assets"

    def __init__(self, name_folder, name_file, scale: float = 1):
        super().__init__()

        self.scale = scale
        self.texture = load_texture(f"{self.BASE_PATH}/{name_folder}/{name_file}.png")

