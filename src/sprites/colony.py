from arcade import SpriteCircle
import arcade


class Colony(SpriteCircle):

    RADIUS = 50
    COLOR = arcade.color.AUBURN

    def __init__(self):
        super().__init__(self.RADIUS, self.__class__.COLOR)
        self.food_collected = 0

