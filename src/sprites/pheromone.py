import  math
from arcade import SpriteCircle
import arcade

from utils import Vector2D


class Pheromone(SpriteCircle):

    RADIUS = 3
    LIFE_TIME = 20
    COEFF_INTENSITY = 200
    COLOR = arcade.color.BLACK

    def __init__(self, intensity: float):
        super().__init__(self.RADIUS, self.__class__.COLOR, soft=True)
        self.intensity = intensity

    @classmethod
    def calcul_intensity(cls, time: float) -> float:
        return cls.COEFF_INTENSITY * math.exp(-time / 15)

    @classmethod
    def from_time(cls, time: float):
        return cls(cls.calcul_intensity(time))

    @classmethod
    def from_ant(cls, ant, base):
        pheromone = cls.from_time(ant.internal_clock)
        pheromone.center_x = ant.center_x
        pheromone.center_y = ant.center_y
        return pheromone


class RedPheromone(Pheromone):

    COLOR = arcade.color.PASTEL_RED
    COEFF_INTENSITY = 100

    def __init__(self, intensity: float):
        super().__init__(intensity)

    @classmethod
    def from_ant(cls, ant):
        return super().from_ant(ant, ant.food)


class BluePheromone(Pheromone):

    COLOR = arcade.color.PERSIAN_BLUE

    def __init__(self, intensity: float):
        super().__init__(intensity)

    @classmethod
    def from_ant(cls, ant):
        return super().from_ant(ant, ant.colony)

