import random
import math
from arcade import SpriteCircle
import arcade


class Food(SpriteCircle):

    RADIUS = 4

    def __init__(self, center_x: float, center_y: float, radius: int = 30):
        super().__init__(Food.RADIUS, arcade.color.PISTACHIO, soft=True)
        circle_direction = 2 * math.pi * random.random()
        self.center_x = center_x + random.randint(0, radius) * math.cos(circle_direction)
        self.center_y = center_y + random.randint(0, radius) * math.sin(circle_direction)
        self.on_map = True

    def kill(self):
        self.on_map = False
        super().kill()

    @classmethod
    def create_source(cls,
                      sprite_list,
                      center_x: float,
                      center_y: float,
                      n: int,
                      radius: int = 30):
        for i in range(n):
            sprite_list.append(cls(center_x, center_y, radius))

