from abc import ABC, abstractmethod, abstractclassmethod
from pathlib import Path
import yaml


class BaseConfig(ABC):

    def __init__(self, **kwargs):
        for k, arg in kwargs.items():
            setattr(self, k, arg)

    def __getitem__(self, k: str):
        return getattr(self, k)

    @abstractmethod
    def to_dict(self) -> dict:
        raise NotImplementedError

    @abstractclassmethod
    def from_dict(self, d: dict):
        raise NotImplementedError


class BaseFileConfig(BaseConfig, ABC):

    PATH: Path = None  # must be set in subclass

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def save(self):
        with open(self.PATH, 'w') as f:
            yaml.dump(self.to_dict(), f, indent=4)

    @classmethod
    def load(cls):
        with open(cls.PATH, 'r') as f:
            config = yaml.safe_load(f)
        return cls.from_dict(config)


class WindowConfig(BaseConfig):

    def __init__(self,
                 width: int = 1250,
                 height: int = 700,
                 fullscreen: bool = False,
                 title: str = "PyGAco"):
        super().__init__(width=width,
                         height=height,
                         fullscreen=fullscreen,
                         title=title)

    def to_dict(self) -> dict:
        return {
            'width': self.width,
            'height': self.height,
            'fullscreen': self.fullscreen,
            'title': self.title,
        }

    @classmethod
    def from_dict(cls, d: dict):
        kwargs = dict()
        if d.get('width'):
            kwargs['width'] = d.pop('width')
        if d.get('height'):
            kwargs['height'] = d.pop('height')
        if d.get('title'):
            kwargs['title'] = d.pop('title')
        if d.get('fullscreen'):
            kwargs['fullscreen'] = d.pop('fullscreen')
        return cls(**kwargs)


class SimulationConfig(BaseConfig):

    def __init__(self,
                 map_path: Path = None,
                 update_rate: float = 1 / 30,
                 ant_count: int = 10,
                 ant_velocity: int = 50,
                 map_layer_walls: str = "Walls",
                 map_tile_scaling: float = 1.):
        super().__init__(
            map_path=Path(map_path).resolve() if map_path else None,
            update_rate=update_rate,
            ant_count=ant_count,
            ant_velocity=ant_velocity,
            map_layer_walls=map_layer_walls,
            map_tile_scaling=map_tile_scaling,
        )

    def to_dict(self) -> dict:
        return {
            'map': str(self.map_path),
            'ant_count': self.ant_count,
            'ant_velocity': self.ant_velocity,
            'update_rate': self.update_rate,
            'map_layer_walls': self.map_layer_walls,
            'tile_map_scaling': self.map_tile_scaling,
        }

    @classmethod
    def from_dict(cls, d: dict):
        kwargs = dict()
        if d.get('ant_count'):
            kwargs['ant_count'] = d.pop('ant_count')
        if d.get('ant_velocity'):
            kwargs['ant_velocity'] = d.pop('ant_velocity')
        if d.get('map'):
            kwargs['map_path'] = d.pop('map')
        if d.get('update_rate'):
            kwargs['update_rate'] = 1 / d.pop('update_rate')
        if d.get('map_layer_walls'):
            kwargs['map_layer_walls'] = d.pop('map_layer_walls').strip()
        if d.get('tile_map_scaling'):
            kwargs['map_tile_scaling'] = d.pop('tile_map_scaling')
        return cls(**kwargs)


class AppConfig(BaseFileConfig):

    PATH = Path(__file__).parent.parent / 'settings.yml'

    def __init__(self,
                 window_config=WindowConfig(),
                 simu_config=SimulationConfig()):
        super().__init__(window=window_config, simulation=simu_config)

    def to_dict(self) -> dict:
        return {
            'window': self.window.to_dict(),
            'simulation': self.simulation.to_dict(),
        }

    @classmethod
    def from_dict(cls, d: dict):
        kwargs = dict()
        if d.get('window'):
            kwargs['window_config'] = WindowConfig.from_dict(d.pop('window'))
        if d.get('simulation'):
            kwargs['simu_config'] = SimulationConfig.from_dict(d.pop('simulation'))
        return cls(**kwargs)

