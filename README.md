# PyGaco

**PyGaco** (PyGame Ant colony optimization) is a graphics simulation of an ant colony based on the ACO algorithm.

The [Tiled][tiled] software is used to create a map for the simulation. You can use your own map if you respect [layers convetions naming](#Map).

## Version 0.3.0

## Requirements

 * [arcade][arcade]

## Milestones

|Number|Version|Description|Reached|Results|
|:----:|:-----:|-----------|:-----:|-------|
|1|0.1.X|No map implementation. Ant search the nearest pheromone (or colony/food). [Details](#Milestone-1).|✅|Many ants get lost, no real paths created.|
|2|0.2.X|Add map implementation and a intensity system for pheromones. Ants search the pheromone with he hightest intensity. [Details](#Milestone-2).|✅|Correct, better with a map like `assets/map1.tmx`|
|3|0.3.X|Update intensity system for pheromone. [Details](#Milestone-3).|✅|Quite good, but when a source food is exhausted, the ants follow too long the red pheromones.|


### Milestone 1

 * No map implementation.
 * Ants look for food first if they do not carry any, otherwise the colony. if they see neither, they look for the nearest pheromone. If there is no pheromone visible, they move randomly.

### Milestone 2

 * Add an implementation for map (TileMap).
 * Add intensity system for pheromone: Pheromone have a intensity attribute (replace the `create_at` attribute), at each update, the intensity decreases by 1 and if `intensity == 0`, the pheromone is removed. A new pheromone has an intensity function of the distance with the "base" (colony for blue pheromone and the food for red pheromne): `intensity = Pheromone.COEFF_INTENSITY * math.exp(-(distance / 120))`. So, a new blue pheromone have a greater intensity when it's clos to the colony, same for red pheromone with the source food.
 * If the ant search a pheromone, it search the pheromone with the highter intensity.

### Milestone 3

 * Add internal clock for ants: Ants have an internal clock to compute the time of a trajectory. This clock is reset when the ant finds its target (food or colony).
 * Update intensity system: The start intensity no longer depends on the distance but on the internal clock of the ant.

## Customization

### Map

To use custom map, you can create a map with [Tiled][tiled], and specify the file in the config file or as parameters of CLI.


[tiled]: https://www.mapeditor.org/
[arcade]: https://api.arcade.academy/en/latest/index.html

